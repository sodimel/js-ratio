import requests as r
from bs4 import BeautifulSoup as bs
import sys
import re

def get_domain_js(domain, print_dots=False):
    try:
        headers = {'User-Agent': 'js-ratio - https://gitlab.com/sodimel/js-ratio',}
        site = r.get(domain, headers=headers, timeout=10)
        site_content = site.text
        site_size = len(site_content)
        soup = bs(site_content, features="html.parser")


        inline_js_size = 0
        inline_js_scripts = soup.find_all('script', src=False)
        if inline_js_scripts:
            for inline_js in inline_js_scripts:
                if inline_js.string:
                    inline_js_size += len(inline_js.string)

        js_links = []
        for script in soup.find_all('script', src=True):
            js_links.append(script.get('src'))
            if print_dots:
                print(".", end="", flush=True)
        total_js = 0
        for i in range(0, len(js_links)):
            if js_links[i].startswith("//"):
                js_links[i] = "https:" + js_links[i]
            if not js_links[i].startswith("http"):
                js_links[i] = domain + js_links[i]
            total_js += len(r.get(js_links[i], headers=headers, timeout=10).text)

        site_size_without_js = site_size - inline_js_size

        total_js += inline_js_size
        return {"status": "ok", "js_links": js_links, "site_size": site_size, "site_size_without_js": site_size_without_js, "inline_js_size": inline_js_size, "total_js": total_js}
    except r.exceptions.ConnectionError:
        return {"status": "connection failed"}
    except r.exceptions.Timeout:
        return {"status": "timeout"}
    except r.exceptions.InvalidURL:
        return {"status": "des gros nuls"}


if __name__ == "__main__":

    with open(sys.argv[1], "r") as file:
        lines = file.readlines()

    count = 1

    with open("report.csv", "a") as write:
        write.write("Domain, Status, JS RATIO, Site size without js, Total js, Nb js sources, Site size, Inline js size, Js links\n")

    for line in lines:
        domain = "https://" + line.split(",")[1].replace("\r", "").replace("\n", "")

        print(f"[{count}/{len(lines)}] Doing {domain}", end="", flush=True)

        result = get_domain_js(domain, print_dots=True)

        text = domain
        text += f", {result['status']}"

        if result["status"] == "ok":
            js_ratio = str(round(result["total_js"]/result["site_size_without_js"], 2))

            text += f", {js_ratio}"
            text += f", {str(result['site_size_without_js'])}"
            text += f", {str(result['total_js'])}"
            text += f", {str(len(result['js_links']))}"
            text += f", {str(result['site_size'])}"
            text += f", {str(result['inline_js_size'])}, "
            text += ", ".join([js_link for js_link in result["js_links"]])

            print(f" Done ({js_ratio})!")

        elif result["status"] == "connection failed":
            text = domain + ", connection failed"
            print(" Done (X)!")
        elif result["status"] == "timeout":
            text = domain + ", timeout"
            print(" Done (X)!")
        elif result["status"] == "des gros nuls":  # <script src="https:cdn.cookielaw.org/consent/0/OtAutoBlock.js" type="text/javascript"></script>
                                                   # hey nice html rackspace.com -.-'
            text = domain + ", malformed js url"
            print(" Done (X)!")

        with open("report.csv", "a") as write:
            write.write(text + "\n")
        count+=1
