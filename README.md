# Js ratio

How many times is there more javascript than html on webpages today? 15 times? 30?

----

## Requirements

```bash
python3 -m pip install bs4 requests
```

----

## Launch:

This script can:

* take a csv file (id, url) when launched from **command line** and output this data in a csv file named report.csv:
  * Domain, Status, JS RATIO, Site size without js, Total js, Nb js sources, Site size, Inline js size, Js links
* launch the `get_domain_js` function from another script, and get all the results on a nice dict

### CLI

```bash
# download the latest tranco list from https://tranco-list.eu/ ? :)
python3 js_ratio.py /path/to/file.csv
# file.csv format: id,url (for example a tranco export?)
```

Will output a file named "report.csv" containing the js ratio of all the urls you submitted, example:

```
Domain, Status, JS RATIO, Site size without js, Total js, Nb js sources, Site size, Inline js size, Js links
https://google.com, ok, 0.0, 13495, 0, 0, 13495, 0,
```

### From python code

```python
import js_ratio
results = js_ratio.get_domain_js("url");

# results (for url = "https://facebook.com") will be:
# {
#   'status': 'ok',
#   'js_links': [
#     'https://static.xx.fbcdn.net/rsrc.php/v3/yL/r/rhKP1bWP9Zc.js?_nc_x=Ij3Wp8lg5Kz'
#   ],
#   'site_size': 99146,
#   'site_size_without_js': 16011,
#   'inline_js_size': 83135,
#   'total_js': 436956
# }
```